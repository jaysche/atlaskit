import styled from 'styled-components';

const NestedNavigationSplitBackButtonWrapper = styled.div`
    display: flex;
`;

NestedNavigationSplitBackButtonWrapper.displayName = 'NestedNavigationSplitBackButtonWrapper';
export default NestedNavigationSplitBackButtonWrapper;
