import styled from 'styled-components';
import { gridSize } from '../../shared-variables';

const SearchResults = styled.div`
  margin-top: ${gridSize * 3}px;
`;

SearchResults.displayName = 'SearchResults';
export default SearchResults;
