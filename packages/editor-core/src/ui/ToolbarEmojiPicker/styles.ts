import styled from 'styled-components';
import ToolbarButtonDefault from '../ToolbarButton';

// tslint:disable-next-line:variable-name
export const ToolbarButton: any = styled(ToolbarButtonDefault)`
  display: flex;
`;

export const offsetY: number = 8; // Padding between picker and button is 8px
export const marginRight: number = 16; // Padding between right of editor and picker is 16px
