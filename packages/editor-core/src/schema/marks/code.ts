import { MarkSpec } from '../../prosemirror';

export const code: MarkSpec = {
  excludes: 'em strike strong underline emojiQuery mentionQuery textColor',
  inclusive: true,
  parseDOM: [
    { tag: 'code', preserveWhitespace: true },
    { tag: 'tt', preserveWhitespace: true },
    {
      style: 'font-family',
      preserveWhitespace: true,
      getAttrs: (value: string) => (value.toLowerCase().indexOf('monospace') > -1) && null,
    },
    { style: 'white-space',
      preserveWhitespace: true,
      getAttrs: value => value === 'pre' && null
    },
  ],
  toDOM(): [string, any] {
    return ['span', {
      style: 'font-family: monospace; white-space: pre-wrap;',
      class: 'code'
    }];
  }
};
