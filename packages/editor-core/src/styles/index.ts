import {
  akCodeFontFamily,
  akColorB100,
  akColorB400,
  akColorB50,
  akColorN0,
  akColorN20,
  akColorN40,
  akColorN50,
  akColorN500,
  akColorN700,
  akColorN900,
  akZIndexLayer
} from '@atlaskit/util-shared-styles';

export const akEditorCodeFontFamily = akCodeFontFamily;
export const akEditorInactiveForeground = akColorN500;
export const akEditorFocus = akColorB100;
export const akEditorSubtleAccent = akColorN40;
export const akEditorActiveBackground = akColorN500;
export const akEditorActiveForeground = akColorN0;
export const akEditorDropdownActiveBackground = akColorN900;
export const akEditorPopupBackground = akColorN700;
export const akEditorPopupText = akColorB50;
export const akEditorPrimaryButton = akColorB400;
export const akEditorCodeBackground = akColorN20;
export const akEditorCodeBlockPadding = '12px';
export const akEditorCodeInlinePadding = '2px 4px';
export const akEditorFloatingPanelZIndex = akZIndexLayer;
export const akEditorMentionSelected = akColorN50;
