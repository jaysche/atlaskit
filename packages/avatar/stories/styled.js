import styled from 'styled-components';

export const AvatarRow = styled.div `
  margin-left: 5px;
  margin-top: 17px;
`;

export const Example = styled.div`
  border: 1px dotted blue;
  display: inline-block;
  margin: 10px;
  padding: 10px;
  width: 500px;
`;
