export type PresenceType = 'none' | 'online' | 'busy' | 'offline';
export type Size = 'xsmall' | 'small' | 'medium' | 'large' | 'xlarge';
