/* eslint-disable import/prefer-default-export */

export const MOBILE_QUERY = '(max-width: 779px)';
export const DESKTOP_QUERY = '(min-width: 1200px)';
