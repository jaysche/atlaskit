import AkProfilecard from './profilecard';
import AkProfileClient from './api/profile-client';
import AkProfilecardResourced from './profilecard-resourced';
import AkProfilecardTrigger from './profilecard-trigger';

export { AkProfilecard };
export { AkProfilecardTrigger };
export { AkProfileClient };

export default AkProfilecardResourced;
