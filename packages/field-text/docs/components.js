const path = require('path');

module.exports = [
  { name: 'Text Field', src: path.join(__dirname, '../src/index.jsx') },
  { name: 'FieldText', src: path.join(__dirname, '../src/FieldText.jsx') },
];
