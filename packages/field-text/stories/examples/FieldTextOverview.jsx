import React from 'react';
import AkFieldText from '@atlaskit/field-text';

export default (
  <AkFieldText
    placeholder="This is a placeholder"
    label="An overview of a field text"
  />
);
